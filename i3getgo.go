// Name:        i3getgo
// Created:     2019-04-10, @x200
// Last update: 2019-05-08, 11:44:33 @lenovo
// Doc:         Replacement for i3get.py
// Todo:

package main

import (
	"flag"
	"fmt"
	"go.i3wm.org/i3"
	"log"
	"reflect"
)

func main() {
	// connect to i3ipc
	tree, tree_err := i3.GetTree()
	if tree_err != nil {
		log.Fatal(tree_err)
	}

	// get workspace
	workspace := tree.Root.FindFocused(func(n *i3.Node) bool {
		return n.Type == i3.WorkspaceNode
	})
	if workspace == nil {
		log.Fatalf("could not locate workspace")
	}

	// get outputs
	outputs, output_err := i3.GetOutputs()
	if output_err != nil {
		log.Fatal(output_err)
	}

	var output_geometry i3.Rect
	var output_name string
	var output_primary string
	for i, _ := range outputs {
		if outputs[i].CurrentWorkspace == workspace.Name {
			output_geometry = outputs[i].Rect
			output_name = outputs[i].Name
		}
		if outputs[i].Primary {
			output_primary = outputs[i].Name
		}
	}

	// get window
	window := workspace.FindFocused(func(n *i3.Node) bool {
		return n.Focused == true
	})
	if window == nil {
		log.Fatalf("could not locate focused window")
	}

	// set flags
	flag.Bool("t", false, "get window title")
	flag.Bool("i", false, "get window instance")
	flag.Bool("c", false, "get window class")
	flag.Bool("g", false, "get window geometry")
	flag.Bool("W", false, "get workspace geometry")
	flag.Bool("O", false, "get output name")
	flag.Bool("X", false, "get output geometry")
	flag.Bool("w", false, "get workspace name")
	flag.Bool("I", false, "get X window id")
	flag.Bool("P", false, "get primary output name")

	flag.Parse()

	response := struct {
		t, i, c, g, W, X, O, P, w string
		I                         int64
	}{
		window.WindowProperties.Title,
		window.WindowProperties.Instance,
		window.WindowProperties.Class,
		geometry(window.Rect),
		geometry(workspace.Rect),
		geometry(output_geometry),
		output_name,
		output_primary,
		workspace.Name,
		window.Window,
	}

	// parse response struct
	v := reflect.ValueOf(response)
	response_values := make([]string, v.NumField())

	for i := 0; i < v.NumField(); i++ {
		response_values[i] = v.Field(i).String()
	}

	// parse flags
	visitor := func(a *flag.Flag) {
		if a.Value.String() == "true" {
			fmt.Printf("%v\n", v.FieldByName(a.Name))
		}
	}

	// execute
	if flag.NFlag() == 0 {
		flag.Usage()
	} else {
		flag.Visit(visitor)
	}
}

// helper
func geometry(n i3.Rect) string {
	return fmt.Sprintf(
		"x: %8v\ny: %8v\nwidth: %4v\nheight: %2v",
		n.X,
		n.Y,
		n.Width,
		n.Height,
	)
}
